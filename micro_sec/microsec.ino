uint32_t current_micro = 0;
uint32_t pri_micro = 0;
uint16_t sec = 0;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
}

void loop() {
  // put your main code here, to run repeatedly:
  current_micro = micros();
  if(current_micro >= pri_micro + 1000000){
    
    sec = current_micro / 1000000;
    Serial.print("Time : ");
    Serial.print(sec);
    Serial.println(" sec");
    Serial.print("current_micro : ");
    Serial.print(current_micro);
    Serial.print(", pri_micro : ");
    Serial.println(pri_micro);
    pri_micro = current_micro;
  }
 }
